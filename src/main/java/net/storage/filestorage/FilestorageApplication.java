package net.storage.filestorage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootApplication
public class FilestorageApplication {

	public static void main(String[] args) {
		SpringApplication.run(FilestorageApplication.class, args);
		;
	}

}
