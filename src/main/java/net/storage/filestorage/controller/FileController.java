package net.storage.filestorage.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import net.storage.filestorage.dto.FileDTO;
import net.storage.filestorage.dto.FilterDTO;
import net.storage.filestorage.mapper.FileMapper;
import net.storage.filestorage.model.Role;
import net.storage.filestorage.model.User;
import net.storage.filestorage.repository.RoleRepository;
import net.storage.filestorage.service.FileService;
import net.storage.filestorage.service.UserAuthService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/files")
@RequiredArgsConstructor
@Validated
@Api(description = "Контроллер для работы с файлами")
public class FileController {

    private final RoleRepository roleRepository;

    private final UserAuthService userAuthService;

    private final FileService fileService;
    private final FileMapper fileMapper;
    @Value("${files.download}")
    private String download;

    @PostMapping(value = "/addrole")
    public Role addRole(@RequestBody Role role) {
        return roleRepository.save(role);
    }

    @PostMapping(value = "/adduser")
    public User addUser(@RequestBody User user) {
        user.setPassword(new BCryptPasswordEncoder().encode(user.getPassword()));
        return userAuthService.addUser(user);
    }


    @GetMapping("/{id}")
    @ApiOperation("Получение файла по ID")
    public FileDTO getFile(@PathVariable("id") Long id) {
        FileDTO fileDTO = fileMapper.toDto(fileService.getFileById(id));
        fileDTO.setDownloadLink(download + fileDTO.getId());
        return fileDTO;
    }

    @PutMapping("/{id}")
    @ApiOperation("Обновление информации о файле по ID")
    public FileDTO updateFile(@Valid @RequestBody FileDTO newFileDTO, @PathVariable Long id) {
        FileDTO fileDTO = fileMapper.toDto(fileService.update(id, newFileDTO.getName(), newFileDTO.getType(), newFileDTO.getSize()));
        fileDTO.setDownloadLink(download + fileDTO.getId());
        return fileDTO;
    }

    @DeleteMapping("/{id}")
    @ApiOperation("Удаление файла по ID")
    public void deleteFile(@PathVariable("id") Long id) {
        fileService.deleteById(id);
    }

    @GetMapping("/all")
    @ApiOperation("Получение всех файлов")
    public List<FileDTO> getAllFiles() {
        List<FileDTO> fileDTOS = fileMapper.listToDto(fileService.getAll());
        fileDTOS.forEach(fileDTO -> {fileDTO.setDownloadLink(download + fileDTO.getId());});
        return fileDTOS;
    }

    @PostMapping("/")
    @ApiOperation("Создание нового файла")
    public FileDTO createFile(@Valid @RequestBody FileDTO newFileDTO) {
        FileDTO fileDTO = fileMapper.toDto(fileService.create(newFileDTO.getName(), newFileDTO.getType(), newFileDTO.getSize()));
        fileDTO.setDownloadLink(download + fileDTO.getId());
        return fileDTO;
    }

    @GetMapping("/names")
    @ApiOperation("Получение всех имен файлов")
    public List<String> getAllNames() {
        return fileService.getAllNames();
    }

    @GetMapping("/download/{id}")
    @ApiOperation("Скачивание файла по ID")
    public ResponseEntity<byte[]> downloadFile(@PathVariable("id") Long id) {
        FileDTO fileDTO = fileMapper.toDto(fileService.getFileById(id));
        byte[] bytes = fileService.downloadDocument(id);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CACHE_CONTROL, "no-cache, no-store, must-revalidate");
        headers.add(HttpHeaders.PRAGMA, "no-cache");
        headers.add(HttpHeaders.EXPIRES, "0");
        headers.add(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=" + fileDTO.getName() + '.' + fileDTO.getType());
        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(bytes.length)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(bytes);
    }

    @GetMapping("/download/zip/{listId}")
    @ApiOperation("Скачивание нескольких файлов архивом по их ID")
    public ResponseEntity<byte[]> downloadZip(@PathVariable("listId") Long[] listId) {
        byte[] bytes = fileService.downloadZip(listId);
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CACHE_CONTROL, "no-cache, no-store, must-revalidate");
        headers.add(HttpHeaders.PRAGMA, "no-cache");
        headers.add(HttpHeaders.EXPIRES, "0");
        headers.add(HttpHeaders.CONTENT_DISPOSITION,
                "attachment; filename=newZip.rar");
        return ResponseEntity.ok()
                .headers(headers)
                .contentLength(bytes.length)
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .body(bytes);
    }

    @GetMapping("/all/filter")
    @ApiOperation("Получение всех файлов с указанным фильтром")
    public List<FileDTO> filter(FilterDTO filterDTO) {
        List<FileDTO> fileDTOS = fileMapper.listToDto(fileService.filter(filterDTO));
        fileDTOS.forEach(fileDTO -> {fileDTO.setDownloadLink(download + fileDTO.getId());});
        return fileDTOS;
    }
}
