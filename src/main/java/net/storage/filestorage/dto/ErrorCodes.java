package net.storage.filestorage.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public enum ErrorCodes {
    VALIDATION_FAILED(101),
    INCORRECT_ID(102),
    INCORRECT_ENTERED_DATA(103);
    private int code;

}
