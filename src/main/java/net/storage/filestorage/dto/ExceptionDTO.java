package net.storage.filestorage.dto;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class ExceptionDTO {

    public String errorMessage;
    public int errorCode;
}
