package net.storage.filestorage.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Max;
import javax.validation.constraints.Pattern;
import java.time.LocalDate;


@Getter
@Setter
public class FileDTO {

    private Long id;
    private String name;
    @Pattern(regexp = "txt|docx|pdf|rtf")
    private String type;
    @Max(value=1024*15, message = "Max size of the file - 15Mb")
    private float size;
    private LocalDate dateCreate;
    private LocalDate dateChange;
    private String downloadLink;

}
