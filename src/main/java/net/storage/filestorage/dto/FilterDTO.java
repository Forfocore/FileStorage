package net.storage.filestorage.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
public class FilterDTO {
    private String name;
    private LocalDate from;
    private LocalDate to;
    private List<String> types;
}
