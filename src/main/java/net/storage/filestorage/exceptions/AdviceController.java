package net.storage.filestorage.exceptions;

import net.storage.filestorage.dto.ErrorCodes;
import net.storage.filestorage.dto.ExceptionDTO;
import net.storage.filestorage.exceptions.BadRequestException;
import net.storage.filestorage.exceptions.NoContentException;
import net.storage.filestorage.exceptions.NotFoundException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

@RestControllerAdvice
public class AdviceController {
    @ExceptionHandler(BadRequestException.class)
    public ResponseEntity<String> badRequestException(BadRequestException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(NotFoundException.class)
    public ResponseEntity<String> notFoundException(NotFoundException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(NoContentException.class)
    public ResponseEntity<String> noContentException(NoContentException e) {
        return new ResponseEntity<>(e.getMessage(), HttpStatus.NO_CONTENT);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<ExceptionDTO> validateException() {
        return new ResponseEntity<>(new ExceptionDTO("Invalid type of the file or file is so big.", ErrorCodes.VALIDATION_FAILED.getCode()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(HttpMessageNotReadableException.class)
    public ResponseEntity<ExceptionDTO> messageNotReadableException() {
        return new ResponseEntity<>(new ExceptionDTO("Name, type or size of the file entered incorrectly.", ErrorCodes.INCORRECT_ENTERED_DATA.getCode()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    public ResponseEntity<ExceptionDTO> idIncorrectlyException() {
        return new ResponseEntity<>(new ExceptionDTO("ID entered incorrectly", ErrorCodes.INCORRECT_ID.getCode()), HttpStatus.BAD_REQUEST);
    }
}
