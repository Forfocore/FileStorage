package net.storage.filestorage.mapper;

import net.storage.filestorage.dto.FileDTO;
import net.storage.filestorage.model.FileModel;
import org.mapstruct.Mapper;

import java.util.List;


@Mapper
public interface FileMapper {


    FileDTO toDto(FileModel fileModel);
    FileModel toModel(FileDTO fileDTO);

    List<FileDTO> listToDto(List<FileModel> fileModels);
    List<FileModel> listToModel(List<FileDTO> fileDTOs);

}
