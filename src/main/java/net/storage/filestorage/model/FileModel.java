package net.storage.filestorage.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@Setter
@Table(name = "files")
public class FileModel {

    public FileModel() {

    }

    ;

    public FileModel(String name, String type, float size) {
        this.name = name;
        this.type = type;
        this.size = size;
    }

    ;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id_files")
    private Long id;
    @Column(name = "date_create")
    private LocalDate dateCreate;
    @Column(name = "date_change")
    private LocalDate dateChange;
    @Column(name = "nameF")
    private String name;
    @Column(name = "typeF")
    private String type;
    @Column(name = "sizeF")
    private float size;

    @Column(name = "pathF")
    private String path;
}
