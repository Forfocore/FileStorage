package net.storage.filestorage.repository;

import net.storage.filestorage.model.FileModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface FileRepository extends JpaRepository<FileModel, Long>, JpaSpecificationExecutor<FileModel> {
    @Query("select concat(name, '.', type) from FileModel")
    List<String> getAllNames();
}
