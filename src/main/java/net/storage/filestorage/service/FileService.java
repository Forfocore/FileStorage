package net.storage.filestorage.service;

import lombok.RequiredArgsConstructor;
import net.storage.filestorage.dto.FilterDTO;
import net.storage.filestorage.exceptions.NotFoundException;
import net.storage.filestorage.model.FileModel;
import net.storage.filestorage.repository.FileRepository;
import net.storage.filestorage.specification.FileSpecification;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Service
@RequiredArgsConstructor
public class FileService {


    private final FileRepository fileRepository;
    private final FileSpecification fileSpecification;

    @Value("${files.path}")
    private String path;

    public FileModel getFileById(Long id) {
        FileModel fileModel = fileRepository.findById(id).orElse(null);
        if (fileModel == null) {
            throw new NotFoundException("File is missing");
        }
        return fileModel;
    }

    public FileModel update(Long id, String name, String type, float size) {
        FileModel fileModel = new FileModel(name, type, size);
        fileModel.setId(id);
        fileModel.setDateChange(LocalDate.now());
        fileModel.setDateCreate(getFileById(id).getDateCreate());
        fileModel.setPath(path);
        return fileRepository.save(fileModel);
    }

    public List<FileModel> getAll() {
        List<FileModel> fileModels = fileRepository.findAll();
        return fileModels;
    }

    public void deleteById(Long id) {
        FileModel fileModel = getFileById(id);
        fileRepository.deleteById(id);
    }

    public FileModel create(String name, String type, float size) {
        FileModel fileModel = new FileModel(name, type, size);
        fileModel.setDateCreate(LocalDate.now());
        fileModel.setDateChange(fileModel.getDateCreate());
        fileModel.setPath(path);
        return fileRepository.save(fileModel);
    }


    public byte[] downloadDocument(Long id) {
        try {
            FileModel fileModel = getFileById(id);
            File file = new File(fileModel.getPath() + fileModel.getName() + '.' + fileModel.getType());
            byte[] bytes = Files.readAllBytes(file.toPath());
            return bytes;
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage());

        }
    }

    public byte[] downloadZip(Long[] listId) {

        try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
            try (ZipOutputStream zout = new ZipOutputStream(baos)) {
                for (Long id : listId) {
                    FileModel fileModel = getFileById(id);
                    File file = new File(fileModel.getPath() + fileModel.getName() + '.' + fileModel.getType());
                    zout.putNextEntry(new ZipEntry(file.getName()));
                    zout.write(Files.readAllBytes(file.toPath()));
                    zout.closeEntry();
                }
            }
            return baos.toByteArray();
        } catch (IOException ex) {
            throw new RuntimeException(ex.getMessage());
        }
    }

    public List<String> getAllNames() {
        return fileRepository.getAllNames();
    }


    public List<FileModel> filter(FilterDTO filterDTO) {
        List<Specification> specifications = new ArrayList<>();
        if (filterDTO.getName() != null) {
            specifications.add(fileSpecification.nameLike(filterDTO.getName()));
        }
        if ((filterDTO.getFrom() != null) & (filterDTO.getTo() != null)) {
            specifications.add(fileSpecification.dateBetween(filterDTO.getFrom(), filterDTO.getTo()));
        }
        if (filterDTO.getTypes() != null) {
            specifications.add(fileSpecification.typeIn(filterDTO.getTypes()));
        }
        List<FileModel> fileModels = fileRepository.findAll(fileSpecification.mergeSpecifications(specifications));
        return fileModels;
    }
}
