package net.storage.filestorage.specification;


import net.storage.filestorage.model.FileModel;
import net.storage.filestorage.model.FileModel_;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

import javax.persistence.criteria.Predicate;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
public class FileSpecification {
    public Specification<FileModel> nameLike (String name) {
        return (root, query, criteriaBuilder)
                -> criteriaBuilder.like(root.get(FileModel_.NAME), "%"+name+"%");
}

    public Specification<FileModel> dateBetween (LocalDate dateFrom, LocalDate dateTo) {
        return (root, query, criteriaBuilder)
                -> criteriaBuilder.between(root.get(FileModel_.DATE_CHANGE), dateFrom, dateTo);
    }

    public Specification<FileModel> typeIn (List<String> types) {
        return (root, query, criteriaBuilder)
                -> root.get(FileModel_.TYPE).in(types);
    }

    public Specification<FileModel> mergeSpecifications(List<Specification> specifications) {
        return (root, query, cb) -> {
            List<Predicate> predicates = new ArrayList<>();

            specifications.forEach(specification -> predicates.add(specification.toPredicate(root, query, cb)));

            return cb.and(predicates.toArray(new Predicate[0]));

        };
    }
}
