create sequence hibernate_sequence start 1 increment 1;

create table files (
    id_files int8 not null,
    date_change date,
    date_create date,
    nameF varchar(255),
    pathF varchar(255),
    sizeF decimal,
    typeF varchar(255),
    primary key (id_files)
    );

create table users (
    id_users serial not null,
    username varchar(255),
    password varchar(255),
    fk_role_id int4,
    primary key (id_users)
    );

create table roles (
    role_id  serial not null,
    rolename varchar(255),
    primary key (role_id)
    );

alter table users add constraint FK217q85dy9gx4q1rn3ncj4l94o foreign key (fk_role_id) references roles;