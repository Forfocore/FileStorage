package net.storage.filestorage.service;

import net.storage.filestorage.model.FileModel;
import net.storage.filestorage.repository.FileRepository;
import net.storage.filestorage.specification.FileSpecification;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.mockito.ArgumentMatchers.*;


@ExtendWith(MockitoExtension.class)
@SpringBootTest
class FileServiceTest {
    public FileServiceTest () {
    }

    @Mock
    FileRepository fileRepository;

    @Mock
    FileSpecification fileSpecification;



    @Test
    void update() {
        String name = "name";
        String type = "type";
        float size = 1000;

    }

    @Test
     void create() {
        String name = "name";
        String type = "txt";
        float size = 1000;

        FileModel fileModel = new FileModel(name,type,size);
        Mockito.when(fileRepository.save(any(FileModel.class))).thenReturn(fileModel);
        FileService fileService = new FileService(fileRepository,fileSpecification);
        fileModel = fileService.create(fileModel.getName(), fileModel.getType(), fileModel.getSize());
        Assert.assertEquals(fileModel.getName(), name);
        Assert.assertEquals(fileModel.getType(), type);
        Assert.assertEquals(fileModel.getSize(), size, 1024*15);
        Mockito.verify(fileRepository, Mockito.times(1)).save(argThat(fm -> fm.getName().equals(name) && fm.getType().equals(type) && fm.getSize() == size));
    }
}